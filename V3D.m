(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



BeginPackage["V3D`"];


Begin["`Priv\[EAcute]`"];


V3D`make2560::usage="make2560[img]";
V3D`removeNull::usage="removeNull[v]";
V3D`afficheLignes::usage="afficheLignes[p2d]";
V3D`lut2coord::usage="lut2coord[ic,ip]\nlut2coord[ic,{ipw,iph}]";
V3D`axesMonde::usage="axesMonde[s_:100] axes du monde, taille s. Remplace g0";



make2560[img_]:=ImageResize[img,ImageDimensions[img]*{2560/1920,1080/1080}];
removeNull[v_]:=Select[v,(#=!=Null)&];
afficheLignes[p2d_]:={Line/@removeNull/@p2d,Line/@removeNull/@Transpose[p2d]};
lut2coord[ic_,ip_]:=Module[{},{
Table[Map[(#1*Append[ImageDimensions[ip[[i]]]-1,1])&,ImageData[ic[[i]]],{2}
],{i,Length[ic]}],
Table[Map[(#1*Append[ImageDimensions[ic[[i]]]-1,1])&,ImageData[ip[[i]]],{2}
],{i,Length[ip]}]
}];
lut2coord[ic_,{ipw_,iph_}]:=Module[{},{
Table[Map[(#1*Append[{ipw,iph}-1,1])&,ImageData[ic[[i]]],{2}
],{i,Length[ic]}]
}];
axesMonde[s_:100]:=Graphics3D[{
Black,Sphere[{0,0,0},s/20],
Red,Arrow[{{0,0,0},{s,0,0}}],
Green,Arrow[{{0,0,0},{0,s,0}}],
Blue,Arrow[{{0,0,0},{0,0,s}}]
}];


h=\!\(\*
TagBox[
RowBox[{"(", "", GridBox[{
{"h1", "h2", "h3"},
{"h4", "h5", "h6"},
{"h7", "h8", "h9"}
},
GridBoxAlignment->{"Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.7]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}}], "", ")"}],
Function[BoxForm`e$, MatrixForm[BoxForm`e$]]]\);
w=\!\(\*
TagBox[
RowBox[{"(", "", GridBox[{
{"w1", "0", "w3"},
{"0", "w1", "w4"},
{"w3", "w4", "w2"}
},
GridBoxAlignment->{"Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.7]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}}], "", ")"}],
Function[BoxForm`e$, MatrixForm[BoxForm`e$]]]\);   (* w1=1; w2=f\.b2+u\.b2+v\.b2; w3=-u; w4=-v *)lignes[{{h1_,h2_,h3_},{h4_,h5_,h6_},{h7_,h8_,h9_}}]={
Coefficient[h[[All,1]].w.h[[All,2]],{w1,w2,w3,w4}],
Coefficient[h[[All,1]].w.h[[All,1]]-h[[All,2]].w.h[[All,2]],{w1,w2,w3,w4}]
};


V3D`calibPlan::usage="calibPlan[h1,h2] Effectue un calibrage planaire \[AGrave] partir de deux homographies. Les homographies associe un plan image avec deux plans checkerboard. Note: on pourrait permettre plus que deux homographies...\nRetourne le centre cx,cy et la distance focale d. yo";
SyntaxInformation[calibPlan]={"ArgumentsPattern"->{_,_}};


calibPlan[h1_,h2_]:=Module[{u,w,v,omega,cx,cy,d},(
{u,w,v}=SingularValueDecomposition[Join[lignes[h1],lignes[h2]]];
omega=v[[All,-1]]//Chop;
omega=omega/omega[[1]]; (* w1=1 *)
cx=-omega[[3]];
cy=-omega[[4]];
d=Sqrt[omega[[2]]-cx^2-cy^2];
<|"cx"->cx,"cy"->cy,"d"->d|>
)]


V3D`rot2aa::usage="rot2aa[rot] extrait d'une matrice de rotation rot l'axe et l'angle de rotation autour de cet axe";
SyntaxInformation[rot2aa]={"ArgumentsPattern"->{_}};


rot2aa[rot_]:=Module[{ev,evec,pp,axe,angle},(
{ev,evec}=Eigensystem[rot]//Chop;
pp=Position[Im[ev],0][[1,1]];
axe=evec[[pp]];
angle=Arg[Delete[ev,pp]];
angle=SortBy[angle,(-Tr[rot.RotationMatrix[-#,axe]])&];
{axe,angle[[1]]}
)];


V3D`paramPlan::usage="paramPlan[h0,params] calcule la pose du plan checkerboard de l'homographie h0, \[AGrave] partir des param\[EGrave]tres internes params (cx,cy,d).";
SyntaxInformation[paramPlan]={"ArgumentsPattern"->{_,_}};


paramPlan[h0_,params_]:=Module[{h,r1,r2,r3,rt,plan1dist,u,w,v,rot,pp,center,axe,angle,ev,evec,Ki,nRot},(

Ki=Inverse[{{params["d"],0,params["cx"]},{0,params["d"],params["cy"]},{0,0,1}}];
h=Ki.h0;
h=h/Norm[h[[All,1]]];
(*r1=h[[All,1]];
r2=h[[All,2]];*)
{r1,r2,rt}=Transpose[h];
r3=Cross[r1,r2];
(* ajuste la matrice de rotation *)
{u,w,v}=SingularValueDecomposition[Transpose[{r1,r2,r3}]];
rot=u.Transpose[v];
center=-Transpose[rot].h[[All,3]];
{axe,angle}=rot2aa[rot];
<|"center"->center,"axe"->axe,"angle"->angle|>
)];


V3D`findPositionPlans::usage="findPositionPlans[{p2d1,p2d2,p2d3}] calibre le plan image 2 \[AGrave] partir des plans 1 et 3. Les points de p2d1, p2d2 et p2d3 sont correspondants.\nLes trois plans retourn\[EAcute] contienent chacun la pose (center, axe, angle)";
SyntaxInformation[findPositionPlans]={"ArgumentsPattern"->{{_,_,_}}};


findPositionPlans[{p2d1_,p2d2_,p2d3_}]:=Module[{err,h12,h32,paramp1,paramp2,paramp3},(
{err,h12}=FindGeometricTransform[p2d2,p2d1,Method->"Linear"];
{err,h32}=FindGeometricTransform[p2d2,p2d3,Method->"Linear"];
{h12,h32}=Chop[TransformationMatrix[#]]&/@{h12,h32};
h12=Sign[(h12.Append[p2d1[[1]],1])[[3]]]*h12;
h32=Sign[(h32.Append[p2d3[[1]],1])[[3]]]*h32;
paramp2=calibPlan[h12,h32];
paramp1=paramPlan[h12,paramp2];
paramp3=paramPlan[h32,paramp2];
paramp2=<|"center"->{paramp2["cx"],paramp2["cy"],-paramp2["d"]},"axe"->{0,0,1},"angle"->0|>;
{paramp1,paramp2,paramp3}
)];


V3D`findPositionPlansPairs::usage="findPositionPlansPairs[{p2d1,p2d21,p2d23,p2d3}] calibre le plan image 2 \[AGrave] partir des plans 1 et 3. Les correspondances entre les plans 1 et 2 sont donn\[EAcute]es par p2d1 et p2d21, alors que les correspondances entre les plans 2 et 3 sont donn\[EAcute]es par p2d23 et p2d3\nLes trois plans retourn\[EAcute] contienent chacun la pose (center, axe, angle)";
SyntaxInformation[findPositionPlans]={"ArgumentsPattern"->{{_,_,_}}};


findPositionPlansPairs[{p2d1_,p2d21_,p2d23_,p2d3_}]:=Module[{err,h12,h32,paramp1,paramp2,paramp3},(
{err,h12}=FindGeometricTransform[p2d21,p2d1,Method->"Linear"];
{err,h32}=FindGeometricTransform[p2d23,p2d3,Method->"Linear"];
{h12,h32}=Chop[TransformationMatrix[#]]&/@{h12,h32};
h12=Sign[(h12.Append[p2d1[[1]],1])[[3]]]*h12;
h32=Sign[(h32.Append[p2d3[[1]],1])[[3]]]*h32;
paramp2=calibPlan[h12,h32];
paramp1=paramPlan[h12,paramp2];
paramp3=paramPlan[h32,paramp2];
paramp2=<|"center"->{paramp2["cx"],paramp2["cy"],-paramp2["d"]},"axe"->{0,0,1},"angle"->0|>;
{paramp1,paramp2,paramp3}
)];


V3D`param2plan::usage="param2plan[param]";
V3D`plan2rt::usage="plan2rt[plan]";
V3D`plan2plan::usae="plan2plan[plana,planb,planc] envoi le plan c relatif au plan b pour devenir relatif au plan a";
V3D`planTo3d::usage="planTo3d[plan,xyres,step]";
V3D`getRot::usage="getRot[p]";
V3D`getRotDiff::usage="getRotDiff[p1,p2]";
V3D`movePlanCentreOp::uasge="movePlanCentreOp[plan,{x,y,z}]";


param2plan[param_]:=Module[{r,normal,orig,dist},(
r=RotationMatrix[param["angle"],param["axe"]];
normal=r[[All,3]];
dist=normal.(r.param["center" ]);
<|"orig"->(*dist*normal*)-r.param["center"],"u"->r[[All,1]],"v"->r[[All,2]],"n"->normal,"eq"->Append[normal,dist],"angle"->param["angle"],"axe"->param["axe"]|>
)];
plan2rt[plan_]:=Module[{r,t},(
r=Transpose[{plan["u"],plan["v"],plan["n"]}];
AffineTransform[{r,plan["orig"]}]
)]
plan2plan[plana_,planb_,planc_]:=Module[{rta,rtb,rtab,rab,m,p,axe,angle},(
rta=plan2rt[plana];
rtb=plan2rt[planb];
rtab=rta.InverseFunction[rtb];
m=TransformationMatrix[rtab];
rab=m[[1;;3,1;;3]];
p=<|"orig"->rtab[planc["orig"]],"u"->rab.planc["u"],"v"->rab.planc["v"],"n"->rab.planc["n"]|>;
p["eq"]=Append[p["n"],-p["orig"].p["n"]];
{axe,angle}=rot2aa[Transpose[{p["u"],p["v"],p["n"]}]];
p ["axe"]=axe;
p["angle"]=angle;
p
)]
planTo3d[plan_,xyres_,step_:100]:=Table[plan["orig"]+plan["u"]*x+plan["v"]*y,{y,1,xyres[[2]],step},{x,1,xyres[[1]],step}];
getRot[p_]:=Transpose[{p["u"],p["v"],p["n"]}];
getRotDiff[p1_,p2_]:=rot2aa[getRot[p1].Inverse[getRot[p2]]];
movePlanCentreOp[plan_,{x_,y_,z_}]:=Module[{axer,angl,rab,p,axe,angle},(
axer=Cross[{x,y,z},{0,0,1}];
angl=VectorAngle[{x,y,z},{0,0,1}];
rab=RotationTransform[angl,axer];
p=<|"orig"->rab[plan["orig"]],"u"->rab[plan["u"]],"v"->rab[plan["v"]],"n"->rab[plan["n"]]|>;
p["eq"]=Append[p["n"],-p["orig"].p["n"]];
{axe,angle}=rot2aa[Transpose[{p["u"],p["v"],p["n"]}]];
p["axe"]=axe;
p["angle"]=angle;
p
)]


V3D`uvPlan2xyz::usage="uvPlan2xyz[plan,{u,v}]";
V3D`atan::usage="atan[x,z] qui fonctionne m\[EHat]me avec [0,0]";
V3D`xyz2alphaBeta::usage="xyz2alphaBeta[{x,y,z}]";
V3D`xyz2alphaBetaR::usage="xyz2alphaBetaR[{x,y,z}], Null retourne {0,0,-1}";
V3D`xyz2alphaBetaRListe::usage="xyz2alphaBetaRListe[v_] applique xyz2alphaBetaR sur une liste v de points";
V3D`alphaBeta2xyz::usage="alphaBeta2xyz[{alpha,beta}] accepte les Missing[]";
V3D`getxyz::usage="getxyz[plan,{x,y,e}]";
V3D`getAlphaBeta::usage="getAlphaBeta[plan,{x,y,z}]";
V3D`intersecte::usage="intersecte[{p1,p2,p3,p4},{{x1,y1,z1},{x2,y2,z2}}]";
V3D`intersecteProj::usage="intersecteProj[{p1,p2,p3,p4},{{x1,y1,z1},{x2,y2,z2}}] intersecte un plan {p1,p2,p3,p4}.{x,y,z,1}==0 avec la ligne p1,p2. retourne le point 3d et sa distance par rapport au depart p1 dans la direction p2.";
V3D`replan::usage="replan[plan,p3d] accepte Missing[]";
V3D`getp3dfromAlphaBeta::usage="getp3dfromAlphaBeta[plan,alpha,beta]";
V3D`getMatchfromAlphaBeta::usage="getMatchfromAlphaBeta[plan,alpha,beta]";


uvPlan2xyz[plan_,{u_,v_}]:=plan["orig"]+plan["u"]*u+plan["v"]*v;
atan[0,0]:=0;
atan[0.,0.]:=0;
atan[x_,z_]:=ArcTan[x,z];
xyz2alphaBeta[{x_,y_,z_}]:={atan[z,Norm[{x,y}]],Mod[-atan[x,y],2Pi]};
xyz2alphaBetaR[{x_,y_,z_}]:=Append[xyz2alphaBeta[Normalize[{x,y,z}]],Norm[{x,y,z}]];
xyz2alphaBetaR[Null]:={0.,0.,-1.};
xyz2alphaBetaRListe[v_]:=xyz2alphaBetaR/@v;
alphaBeta2xyz[{alpha_,beta_}]:=Normalize[{Sin[alpha]*Cos[beta],-Sin[alpha]*Sin[beta],Cos[alpha]}];
alphaBeta2xyz[{Missing[],_}]:=Missing[];
alphaBeta2xyz[Missing[]]:=Missing[];
getxyz[plan_,{x_,y_,e_}]:=plan["orig"]+plan["u"]*x+plan["v"]*y;
getAlphaBeta[plan_,{x_,y_,z_}]:=xyz2alphaBeta[Normalize[plan["orig"]+plan["u"]*x+plan["v"]*y]];
intersecte[{p1_,p2_,p3_,p4_},{{x1_,y1_,z1_},{x2_,y2_,z2_}}]=Simplify[{{(1-q) x1+q x2,(1-q) y1+q y2,(1-q) z1+q z2},q}/.Solve[{p1,p2,p3,p4}.{(1-q) x1+q x2,(1-q) y1+q y2,(1-q) z1+q z2,1}==0,q][[1]]];
intersecteProj[{p1_,p2_,p3_,p4_},{{x1_,y1_,z1_},{x2_,y2_,z2_}}]={Join[intersecte[{p1,p2,p3,p4},{{x1,y1,z1},{x2,y2,z2}}][[1]]*(p1 (x1-x2)+p2 (y1-y2)+p3 (z1-z2)),{p1 (x1-x2)+p2 (y1-y2)+p3 (z1-z2)}],
intersecte[{p1,p2,p3,p4},{{x1,y1,z1},{x2,y2,z2}}][[2]]};
replan[plan_,p3d_]:={#.plan["u"],#.plan["v"]}&[p3d-plan["orig"]];
replan[plan_,Missing[]]:=Missing[];
getp3dfromAlphaBeta[plan_,alpha_,beta_]:=Module[{inter},(
inter=Quiet[intersecte[plan["eq"],{{0,0,0},alphaBeta2xyz[{alpha,beta}]}]];
inter= inter/.{ComplexInfinity->0.};
If[inter[[2]]>0.,inter[[1]],{Null,Null,Null}]
)];
getMatchfromAlphaBeta[plan_,alpha_,beta_]:=Module[{p},(
p=getp3dfromAlphaBeta[plan,alpha,beta];
If[Not[p==={Null,Null,Null}],replan[plan,p],{Null,Null}]
)]


End[];


EndPackage[ ];



