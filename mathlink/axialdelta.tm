

void axialinit P(( void));
void axialuninit P(( void));
void axialtrouvedz P(( void));
void axiallines P(( void));
void axialalligne P(( void));


:Begin:
:Function:       axialinit
:Pattern:        AxialInit[a_List,b_List,u_Integer,v_Integer]
:Arguments:      {a,b,u,v}
:ArgumentTypes:  {Manual}
:ReturnType:     Manual
:End:
:Evaluate: AxialInit::usage = "AxialInit[a,b]. a est un tableau de Real64 et b est un tableau de Real64"


:Begin:
:Function:       axiallines
:Pattern:        AxialLines[a_List]
:Arguments:      {a}
:ArgumentTypes:  {Manual}
:ReturnType:     Manual
:End:
:Evaluate: AxialLines::usage = "AxialLines[a]. a est un tableau de Real64"


:Begin:
:Function:       axialuninit
:Pattern:        AxialUninit[]
:Arguments:      {}
:ArgumentTypes:  {Manual}
:ReturnType:     Manual
:End:
:Evaluate: AxialUninit::usage = "AxialUninit[]"


:Begin:
:Function:       axialtrouvedz
:Pattern:        AxialTrouveDz[pos_List,m_Real]
:Arguments:      {pos,m}
:ArgumentTypes:  {Manual}
:ReturnType:     Manual
:End:
:Evaluate: AxialTrouveDz::usage = "AxialTrouveDz[pos,m]. pos est une tableau de real64"


:Begin:
:Function:       axialalligne
:Pattern:        AxialAlligne[pos_List, u_Integer]
:Arguments:      {pos,u}
:ArgumentTypes:  {Manual}
:ReturnType:     Manual
:End:
:Evaluate: AxialAlligne::usage = "AxialAlligne[pos,u]. pos est une tableau de real64"


