
//
// Dans mathematica:
//   Install["imagetest"]
// et la fonction ImageTest[i1,i2] apparaitra
//

#include "wstp.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h> // pour clock()

typedef struct {
    double *data;
    int *dims;
    char **heads;
    int depth;
} realmatrix;

typedef struct {
    int *data;
    int *dims;
    char **heads;
    int depth;
} intmatrix;
double clock() {
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return(tv.tv_sec+tv.tv_usec/1000000.0);
}
double accesRealMatrix(realmatrix *g,int *pos) {
	int offset=0;
    for(int i=0;i<g->depth;i++) {
        offset=offset*g->dims[i]+(pos[i]-1);
    }
    //printf("offset = %6d  : %12.4f\n",offset,g->data[offset]);
    return g->data[offset];
}
int accesIntMatrix(intmatrix *g,int *pos) {
	int offset=0;
    for(int i=0;i<g->depth;i++) {
        offset=offset*g->dims[i]+(pos[i]-1);
    }
    //printf("offset = %6d  : %12.4f\n",offset,g->data[offset]);
    return g->data[offset];
}
//retourne les valeurs des pixels dans les luts
void getMc(realmatrix *g, int *pos, double *c){
	int ind[4];
	for(int i=0;i<3;i++){
	ind[i]=pos[i];
	}
	for(int i=0;i<3;i++){
		ind[3]=i+1;
		c[i]=accesRealMatrix(g,ind);
	}
}
void getLine(realmatrix *g, int *pos, double *c){
	int ind[4];
	ind[0]=pos[0];
	ind[1]=pos[1];
	for(int i=0;i<2;i++){
		for(int j=0;j<3;j++){
			ind[2]=i+1;
			ind[3]=j+1;
			c[i*3+j]=accesRealMatrix(g,ind);
		}
	}
}
void accesRealMatrixTriples(realmatrix *g, int *pos, double *c, double *f){
	int ind[4];
	ind[1]=pos[3];
	ind[2]=pos[2];

	ind[0]=pos[0];
	for(int i=0;i<3;i++){
		ind[3]=i+1;
		c[i]=accesRealMatrix(g,ind);
	}
	ind[0]=pos[1];
	for(int i=0;i<3;i++){
		ind[3]=i+1;
		f[i]=accesRealMatrix(g,ind);
	}
}
void getxyz(double *c,  double *pc, double *cxyz){
	for(int i=0;i<3;i++){
		cxyz[i]=pc[i]+pc[3+i]*c[0]+pc[6+i]*c[1];
	}
}
double getdistaxe(double *cxyz, double *fxyz){
	double num=cxyz[0]*fxyz[1]-cxyz[1]*fxyz[0];
	double denum=pow(cxyz[0]-fxyz[0],2)+pow(cxyz[1]-fxyz[1],2);
	return abs(num/sqrt(denum));
}
double getDzAxe(double *cxyz, double *fxyz){
	double x1=cxyz[0];
	double y1=cxyz[1];
	double z1=cxyz[2];
	double x2=fxyz[0];
	double y2=fxyz[1];
	double z2=fxyz[2];

	return (x2*x2*z1+x1*x1*z2-x1*x2*(z1+z2)+(y1-y2)*(-(y2*z1)+y1*z2))/(x1*x1-2*x1*x2+x2*x2+(y1-y2)*(y1-y2));
}
bool checkLineisZero(double *f){
	return f[0]==0&&f[1]==0&&f[2]==0&&f[3]==0&&f[4]==0&&f[5]==0;
}
double calcNorm(double *c){
	return sqrt(c[0]*c[0]+c[1]*c[1]+c[2]*c[2]);
}
void normalize(double *c, double *f){
	double n=calcNorm(c);
	for(int i=0;i<3;i++){f[i]=c[i]/n;}
}
void substractPt3D(double *c, double *f, double *p){
	for(int i=0;i<3;i++){p[i]=c[i]-f[i];}
}
double dotProduct(double *c, double *f){
	double somme=0;
	for(int i=0;i<3;i++){somme+=c[i]*f[i];}
	return somme;
}
void multiplyPt3D(double *c, double m, double *f){
	for(int i=0;i<3;i++){f[i]=c[i]*m;}
}
double coutPtLigne(double *c, double *f){
	double ligne1[3];
	double ligne2[3];
	for(int i=0;i<3;i++){ligne1[i]=f[i];}
	for(int i=0;i<3;i++){ligne2[i]=f[i+3];}
	double pt1[3];
	double pt2[3];
	substractPt3D(c,ligne1,pt1);
	substractPt3D(ligne2,ligne1,pt2);
	double pt3[3];
	normalize(pt1,pt3);
	normalize(pt2,pt2);
	double m=1-dotProduct(pt3,pt2);
	double pt4[3];
	multiplyPt3D(pt1,m,pt4);
	return calcNorm(pt4);
}
double calcRayon(int u, int v, int *cx, int *cy){
	return sqrt((u-*cx)*(u-*cx)+(v-*cy)*(v-*cy));
}
void dumpRealMatrix(realmatrix *g) {
    printf("** depth=%d \n",g->depth);
    int i;
    for(i=0;i<g->depth;i++) printf("** dims[%d]=%d\n",i,g->dims[i]);
}
int sizeRealMatrix(realmatrix *g) {
    int sz;
    int i;
    if( g->depth==0 ) return 0;
    sz=g->dims[0];
    for(i=1;i<g->depth;i++) sz*=g->dims[i];
    return sz;
}
// return 0 si ok, -1 si err
int readRealMatrix(realmatrix *g)
{
    int type;
    const char *str;
    int i;

    if(! WSGetReal64Array(stdlink, &g->data, &g->dims, &g->heads, &g->depth)) {
        printf("Unable to read real64 array\n");
        return -1;
    }

    //dumpRealMatrix(g);
    return 0;
}
// return 0 si ok, -1 si err
int readIntMatrix(intmatrix *g)
{
    int type;
    const char *str;
    int i;

    if(! WSGetInteger32Array(stdlink, &g->data, &g->dims, &g->heads, &g->depth)) {
        printf("Unable to read real64 array\n");
        return -1;
    }
    return 0;
}
void freeRealMatrix(realmatrix *g) {
    WSReleaseReal64Array(stdlink, g->data, g->dims, g->heads, g->depth);
}
void freeIntMatrix(intmatrix *g) {
    WSReleaseInteger32Array(stdlink, g->data, g->dims, g->heads, g->depth);
}
/*
// 1 ok, 0 rien a analyser.
int processArgs(void)
{
    int type, nba, in, i, k, nb, count;
    const char *func;
    short in16;
    double rr;
    const char *str;
    const char *sym;
    unsigned char *buf;

    WSFlush(stdlink);
    if( !WSReady(stdlink) ) return(0);

    switch(type=WSGetNext(stdlink)) {
    case WSTKFUNC:
        if( WSGetFunction(stdlink,&func,&nba) ) {
            printf("function '%s' with %d args\n",func,nba);
            // process l interieur de la structure
            for(i=0;i<nba;i++) processArgs();
        }else printf("function error\n");
        WSReleaseSymbol(stdlink,func);
        break;
    case WSTKINT:
        if( WSRawBytesToGet(stdlink,&nb) ) {
            printf("int << raw bytes nb=%d>>\n",nb);
        }
        else{
            printf("Unable to get byte count\n");
        }
        if( WSGetInteger32(stdlink,&in) ) {
            printf("<< integer 32 %d >>\n",in);
        }
        else{
            printf("<< integer error=%d >>\n",WSError(stdlink));
            WSClearError(stdlink);
        }
        break;
    case WSTKREAL:
        if( WSGetReal64(stdlink,&rr) ) {
            printf("<< real %f >>\n",rr);
        }else{
            printf("<< real error=%d >>\n",WSError(stdlink));
            WSClearError(stdlink);
        }
        break;
    case WSTKSTR:
        WSGetString(stdlink,&str);
        printf("<< string %s >>\n",str);
        WSReleaseString(stdlink,str);
        break;
    case WSTKSYM:
        WSGetSymbol(stdlink,&sym);
        printf("<< symbol %s >>\n",sym);
        WSReleaseSymbol(stdlink,sym);
        break;
    case WSTKERR:
        printf("<< ERROR >>\n");
        return(0); // fini les parametres!
        break;
    default:
        printf("unknown type=%d\n",type);
    }
    return(1);
}
*/
realmatrix g;
intmatrix h;
realmatrix k;
int centreOptx;
int centreOpty;
int ok=0;
//
// axialInit[img1,img2]
//
extern void axialinit( void );
void axialinit( void)
{
    WSGetNext(stdlink);
    if( readRealMatrix(&g) ) { printf("error 1\n"); return; }
    WSGetNext(stdlink);
    if( readIntMatrix(&h) ) { printf("error 1\n"); return; }
    WSGetNext(stdlink);
	WSGetInteger32(stdlink,&centreOptx);
    WSGetNext(stdlink);
	WSGetInteger32(stdlink,&centreOpty);
	//printf("%d,%d \n",centreOptx, centreOpty);
    ok=1;
    printf("init done\n");
    WSPutString(stdlink,"init!");
}
extern void axiallines( void );
void axiallines( void)
{
    WSGetNext(stdlink);
    if( readRealMatrix(&k) ) { printf("error 1\n"); return; }
    //printf("lines load done\n");
    WSPutString(stdlink,"Lines!");
}
extern void axialuninit( void );
void axialuninit( void)
{
    // free all
    freeRealMatrix(&g);
    freeIntMatrix(&h);
    freeRealMatrix(&k);
    ok=0;
    printf("uninit done\n");
    WSPutString(stdlink,"uninit!");
}

extern void axialtrouvedz( void );
void axialtrouvedz( void )
{
    double timestart=clock();
	double *m;

    realmatrix input;
	int indices[4];

    if( !ok ) {
        WSPutString(stdlink,"pas de init!");
        return;
    }

    WSGetNext(stdlink);
    if( readRealMatrix(&input) ) { printf("error 1\n"); return; }

    WSGetNext(stdlink);
	WSGetReal64(stdlink,m);

	double somme=0;
	double* c=(double*)malloc(3*sizeof(double));
	double* f=(double*)malloc(3*sizeof(double));
	double* pc=(double*)malloc(12*sizeof(double));
	double* pf=(double*)malloc(12*sizeof(double));
	double* cxyz=(double*)malloc(3*sizeof(double));
	double* fxyz=(double*)malloc(3*sizeof(double));

	for(int k=1;k<=h.dims[0];k++){
		int pos1[2]={k,1};
		int pos2[2]={k,2};
		indices[0]=	accesIntMatrix(&h,pos1);
		indices[1]=	accesIntMatrix(&h,pos2);
		//printf("%d, %d, %d \n",k,indices[0],indices[1]);

		for(int i=0;i<12;i++){pc[i]=input.data[(indices[0]-1)*12+i];}
		for(int i=0;i<12;i++){pf[i]=input.data[(indices[1]-1)*12+i];}
		//printf("%f,%f,%f,%f \n",pc[2],pc[5],pc[8],pc[11]);
		//printf("%f,%f,%f,%f \n",pf[2],pf[5],pf[8],pf[11]);

		double sommeint=0;
		int compteur=0;
		for(int u=1;u<=g.dims[2];u+=2){
			for(int v=1;v<=g.dims[1];v+=2){
				indices[2]=u;
				indices[3]=v;

				double r = calcRayon(u,v,&centreOptx,&centreOpty);
				//double dzreel = r**m;
				double dzreel = 0;

				accesRealMatrixTriples(&g,indices,c,f);
				//printf("%f,%f,%f \n",c[0],c[1],c[2]);
				//printf("%f,%f,%f \n",f[0],f[1],f[2]);

				if((c[2]>0.2)||(f[2]>0.2)||r<10){
					continue;
				}

				getxyz(c,pc,cxyz);
				getxyz(f,pf,fxyz);
				//printf("%f,%f,%f \n",cxyz[0],cxyz[1],cxyz[2]);
				//printf("%f,%f,%f \n",fxyz[0],fxyz[1],fxyz[2]);

				double daxe=getdistaxe(cxyz,fxyz);
				double dz=getDzAxe(cxyz,fxyz);
				//printf("%f \n",daxe);
				double dzcout=abs(dz-dzreel);

				double distPointAxe = sqrt(daxe*daxe+dzcout*dzcout);

				sommeint+=daxe;
				compteur+=1;
			}
		}
		if(compteur!=0){somme+=sommeint/compteur;}
	}
    // free all
	free(c);
	free(f);
	free(pc);
	free(pf);
	free(cxyz);
	free(fxyz);
    freeRealMatrix(&input);
	//retour de la valeur
	WSPutReal64(stdlink,somme);

	double timeend=clock();
    //printf("Timing: %8.2f ms\n",(timeend-timestart)*1000);
}

extern void axialalligne( void );
void axialalligne( void )
{
    double timestart=clock();

    realmatrix input;
	int indices[3];
	int ligne[2];

    if( !ok ) {
        WSPutString(stdlink,"pas de init!");
        return;
    }

    WSGetNext(stdlink);
    if( readRealMatrix(&input) ) { printf("error 1\n"); return; }

    WSGetNext(stdlink);
	WSGetInteger32(stdlink,indices+0);

	double somme=0;
	double* c=(double*)malloc(3*sizeof(double));
	double* f=(double*)malloc(6*sizeof(double));
	double* pc=(double*)malloc(12*sizeof(double));
	double* cxyz=(double*)malloc(3*sizeof(double));

	for(int i=0;i<12;i++){pc[i]=input.data[i];}
	//printf("%f,%f,%f,%f \n",pc[2],pc[5],pc[8],pc[11]);

	int compteur=0;
	for(int u=1;u<=g.dims[2];u+=4){
		for(int v=1;v<=g.dims[1];v+=4){
			indices[2]=u;
			indices[1]=v;
			ligne[1]=u;
			ligne[0]=v;
			getMc(&g,indices,c);
			getLine(&k,ligne,f);
			if(c[2]>0.2||checkLineisZero(f)){
				continue;
			}
			getxyz(c,pc,cxyz);
			//printf("%f,%f,%f \n",cxyz[0],cxyz[1],cxyz[2]);
			//printf("%f,%f,%f,%f,%f,%f \n",f[0],f[1],f[2],f[3],f[4],f[5]);
			compteur+=1;
			somme+=coutPtLigne(cxyz,f);
		}
	}
	somme=somme/compteur;

    // free all
	free(c);
	free(f);
    freeRealMatrix(&input);
	//retour de la valeur
	WSPutReal64(stdlink,somme);

	double timeend=clock();
    //printf("Timing: %8.2f ms\n",(timeend-timestart)*1000);
}


int main(int argc, char* argv[])
{
    return WSMain(argc, argv);
}
