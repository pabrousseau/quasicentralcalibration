/*
 * This file automatically produced by /opt/Mathematica/SystemFiles/Links/WSTP/DeveloperKit/Linux-x86-64/CompilerAdditions/wsprep from:
 *	axialdelta.tm
 * mprep Revision 18 Copyright (c) Wolfram Research, Inc. 1990-2013
 */

#define MPREP_REVISION 18

#include "wstp.h"

int PREPAbort = 0;
int PREPDone  = 0;
long PREPSpecialCharacter = '\0';

WSLINK stdlink = 0;
WSEnvironment stdenv = 0;
WSYieldFunctionObject stdyielder = (WSYieldFunctionObject)0;
WSMessageHandlerObject stdhandler = (WSMessageHandlerObject)0;

extern int PREPDoCallPacket(WSLINK);
extern int PREPEvaluate( WSLINK, char *);
extern int PREPEvaluateString P(( WSLINK, char *));

/********************************* end header *********************************/


# line 3 "axialdelta.tm"
void axialinit P(( void));
void axialuninit P(( void));
void axialtrouvedz P(( void));
void axiallines P(( void));
void axialalligne P(( void));


# line 36 "axialdeltatm.cxx"


void axialinit P(( void));

static int _tr0( WSLINK wslp)
{
	int	res = 0;
	if( !wslp) return res; /* avoid unused parameter warning */

	axialinit();

	res = 1;

	return res;
} /* _tr0 */


void axiallines P(( void));

static int _tr1( WSLINK wslp)
{
	int	res = 0;
	if( !wslp) return res; /* avoid unused parameter warning */

	axiallines();

	res = 1;

	return res;
} /* _tr1 */


void axialuninit P(( void));

static int _tr2( WSLINK wslp)
{
	int	res = 0;
	if( !wslp) return res; /* avoid unused parameter warning */

	axialuninit();

	res = 1;

	return res;
} /* _tr2 */


void axialtrouvedz P(( void));

static int _tr3( WSLINK wslp)
{
	int	res = 0;
	if( !wslp) return res; /* avoid unused parameter warning */

	axialtrouvedz();

	res = 1;

	return res;
} /* _tr3 */


void axialalligne P(( void));

static int _tr4( WSLINK wslp)
{
	int	res = 0;
	if( !wslp) return res; /* avoid unused parameter warning */

	axialalligne();

	res = 1;

	return res;
} /* _tr4 */


static struct func {
	int   f_nargs;
	int   manual;
	int   (*f_func)P((WSLINK));
	const char  *f_name;
	} _tramps[5] = {
		{ 0, 2, _tr0, "axialinit" },
		{ 0, 2, _tr1, "axiallines" },
		{ 0, 2, _tr2, "axialuninit" },
		{ 0, 2, _tr3, "axialtrouvedz" },
		{ 0, 2, _tr4, "axialalligne" }
		};

static const char* evalstrs[] = {
	"AxialInit::usage = \"AxialInit[a,b]. a est un tableau de Real64 e",
	"t b est un tableau de Real64\"",
	(const char*)0,
	"AxialLines::usage = \"AxialLines[a]. a est un tableau de Real64\"",
	(const char*)0,
	"AxialUninit::usage = \"AxialUninit[]\"",
	(const char*)0,
	"AxialTrouveDz::usage = \"AxialTrouveDz[pos,m]. pos est une tablea",
	"u de real64\"",
	(const char*)0,
	"AxialAlligne::usage = \"AxialAlligne[pos,u]. pos est une tableau ",
	"de real64\"",
	(const char*)0,
	(const char*)0
};
#define CARDOF_EVALSTRS 5

static int _definepattern P(( WSLINK, char*, char*, int));

static int _doevalstr P(( WSLINK, int));

int  _PREPDoCallPacket P(( WSLINK, struct func[], int));


int WSInstall( WSLINK wslp)
{
	int _res;
	_res = WSConnect(wslp);
	if (_res) _res = _definepattern(wslp, (char *)"AxialInit[a_List,b_List,u_Integer,v_Integer]", (char *)"{a,b,u,v}", 0);
	if (_res) _res = _doevalstr( wslp, 0);
	if (_res) _res = _definepattern(wslp, (char *)"AxialLines[a_List]", (char *)"{a}", 1);
	if (_res) _res = _doevalstr( wslp, 1);
	if (_res) _res = _definepattern(wslp, (char *)"AxialUninit[]", (char *)"{}", 2);
	if (_res) _res = _doevalstr( wslp, 2);
	if (_res) _res = _definepattern(wslp, (char *)"AxialTrouveDz[pos_List,m_Real]", (char *)"{pos,m}", 3);
	if (_res) _res = _doevalstr( wslp, 3);
	if (_res) _res = _definepattern(wslp, (char *)"AxialAlligne[pos_List, u_Integer]", (char *)"{pos,u}", 4);
	if (_res) _res = _doevalstr( wslp, 4);
	if (_res) _res = WSPutSymbol( wslp, "End");
	if (_res) _res = WSFlush( wslp);
	return _res;
} /* WSInstall */


int PREPDoCallPacket( WSLINK wslp)
{
	return _PREPDoCallPacket( wslp, _tramps, 5);
} /* PREPDoCallPacket */

/******************************* begin trailer ********************************/

#ifndef EVALSTRS_AS_BYTESTRINGS
#	define EVALSTRS_AS_BYTESTRINGS 1
#endif


#if CARDOF_EVALSTRS
static int  _doevalstr( WSLINK wslp, int n)
{
	long bytesleft, charsleft, bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
	long charsnow;
#endif
	char **s, **p;
	char *t;

	s = (char **)evalstrs;
	while( n-- > 0){
		if( *s == 0) break;
		while( *s++ != 0){}
	}
	if( *s == 0) return 0;
	bytesleft = 0;
	charsleft = 0;
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft += bytesnow;
		charsleft += bytesnow;
#if !EVALSTRS_AS_BYTESTRINGS
		t = *p;
		charsleft -= WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
#endif
		++p;
	}


	WSPutNext( wslp, WSTKSTR);
#if EVALSTRS_AS_BYTESTRINGS
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft -= bytesnow;
		WSPut8BitCharacters( wslp, bytesleft, (unsigned char*)*p, bytesnow);
		++p;
	}
#else
	WSPut7BitCount( wslp, charsleft, bytesleft);
	p = s;
	while( *p){
		t = *p; while( *t) ++t;
		bytesnow = t - *p;
		bytesleft -= bytesnow;
		t = *p;
		charsnow = bytesnow - WSCharacterOffset( &t, t + bytesnow, bytesnow);
		/* assert( t == *p + bytesnow); */
		charsleft -= charsnow;
		WSPut7BitCharacters(  wslp, charsleft, *p, bytesnow, charsnow);
		++p;
	}
#endif
	return WSError( wslp) == WSEOK;
}
#endif /* CARDOF_EVALSTRS */


static int  _definepattern( WSLINK wslp, char *patt, char *args, int func_n)
{
	WSPutFunction( wslp, "DefineExternal", (long)3);
	  WSPutString( wslp, patt);
	  WSPutString( wslp, args);
	  WSPutInteger( wslp, func_n);
	return !WSError(wslp);
} /* _definepattern */


int _PREPDoCallPacket( WSLINK wslp, struct func functable[], int nfuncs)
{
	int len;
	int n, res = 0;
	struct func* funcp;

	if( ! WSGetInteger( wslp, &n) ||  n < 0 ||  n >= nfuncs) goto L0;
	funcp = &functable[n];

	if( funcp->f_nargs >= 0
	&& ( ! WSTestHead(wslp, "List", &len)
	     || ( !funcp->manual && (len != funcp->f_nargs))
	     || (  funcp->manual && (len <  funcp->f_nargs))
	   )
	) goto L0;

	stdlink = wslp;
	res = (*funcp->f_func)( wslp);

L0:	if( res == 0)
		res = WSClearError( wslp) && WSPutSymbol( wslp, "$Failed");
	return res && WSEndPacket( wslp) && WSNewPacket( wslp);
} /* _PREPDoCallPacket */


wsapi_packet PREPAnswer( WSLINK wslp)
{
	wsapi_packet pkt = 0;
	int waitResult;

	while( ! PREPDone && ! WSError(wslp)
		&& (waitResult = WSWaitForLinkActivity(wslp),waitResult) &&
		waitResult == WSWAITSUCCESS && (pkt = WSNextPacket(wslp), pkt) &&
		pkt == CALLPKT)
	{
		PREPAbort = 0;
		if(! PREPDoCallPacket(wslp))
			pkt = 0;
	}
	PREPAbort = 0;
	return pkt;
} /* PREPAnswer */



/*
	Module[ { me = $ParentLink},
		$ParentLink = contents of RESUMEPKT;
		Message[ MessageName[$ParentLink, "notfe"], me];
		me]
*/


static int refuse_to_be_a_frontend( WSLINK wslp)
{
	int pkt;

	WSPutFunction( wslp, "EvaluatePacket", 1);
	  WSPutFunction( wslp, "Module", 2);
	    WSPutFunction( wslp, "List", 1);
		  WSPutFunction( wslp, "Set", 2);
		    WSPutSymbol( wslp, "me");
	        WSPutSymbol( wslp, "$ParentLink");
	  WSPutFunction( wslp, "CompoundExpression", 3);
	    WSPutFunction( wslp, "Set", 2);
	      WSPutSymbol( wslp, "$ParentLink");
	      WSTransferExpression( wslp, wslp);
	    WSPutFunction( wslp, "Message", 2);
	      WSPutFunction( wslp, "MessageName", 2);
	        WSPutSymbol( wslp, "$ParentLink");
	        WSPutString( wslp, "notfe");
	      WSPutSymbol( wslp, "me");
	    WSPutSymbol( wslp, "me");
	WSEndPacket( wslp);

	while( (pkt = WSNextPacket( wslp), pkt) && pkt != SUSPENDPKT)
		WSNewPacket( wslp);
	WSNewPacket( wslp);
	return WSError( wslp) == WSEOK;
}


int PREPEvaluate( WSLINK wslp, char *s)
{
	if( PREPAbort) return 0;
	return WSPutFunction( wslp, "EvaluatePacket", 1L)
		&& WSPutFunction( wslp, "ToExpression", 1L)
		&& WSPutString( wslp, s)
		&& WSEndPacket( wslp);
} /* PREPEvaluate */


int PREPEvaluateString( WSLINK wslp, char *s)
{
	int pkt;
	if( PREPAbort) return 0;
	if( PREPEvaluate( wslp, s)){
		while( (pkt = PREPAnswer( wslp), pkt) && pkt != RETURNPKT)
			WSNewPacket( wslp);
		WSNewPacket( wslp);
	}
	return WSError( wslp) == WSEOK;
} /* PREPEvaluateString */


void PREPDefaultHandler( WSLINK wslp, int message, int n)
{
	switch (message){
	case WSTerminateMessage:
		PREPDone = 1;
	case WSInterruptMessage:
	case WSAbortMessage:
		PREPAbort = 1;
	default:
		return;
	}
}


static int _WSMain( char **argv, char **argv_end, char *commandline)
{
	WSLINK wslp;
	int err;

	if( !stdenv)
		stdenv = WSInitialize( (WSEnvironmentParameter)0);

	if( stdenv == (WSEnvironment)0) goto R0;

	if( !stdhandler)
		stdhandler = (WSMessageHandlerObject)PREPDefaultHandler;


	wslp = commandline
		? WSOpenString( stdenv, commandline, &err)
		: WSOpenArgcArgv( stdenv, (int)(argv_end - argv), argv, &err);
	if( wslp == (WSLINK)0){
		WSAlert( stdenv, WSErrorString( stdenv, err));
		goto R1;
	}

	if( stdyielder) WSSetYieldFunction( wslp, stdyielder);
	if( stdhandler) WSSetMessageHandler( wslp, stdhandler);

	if( WSInstall( wslp))
		while( PREPAnswer( wslp) == RESUMEPKT){
			if( ! refuse_to_be_a_frontend( wslp)) break;
		}

	WSClose( wslp);
R1:	WSDeinitialize( stdenv);
	stdenv = (WSEnvironment)0;
R0:	return !PREPDone;
} /* _WSMain */


int WSMainString( char *commandline)
{
	return _WSMain( (charpp_ct)0, (charpp_ct)0, commandline);
}

int WSMainArgv( char** argv, char** argv_end) /* note not FAR pointers */
{   
	static char FAR * far_argv[128];
	int count = 0;
	
	while(argv < argv_end)
		far_argv[count++] = *argv++;
		 
	return _WSMain( far_argv, far_argv + count, (charp_ct)0);

}


int WSMain( int argc, char **argv)
{
 	return _WSMain( argv, argv + argc, (char *)0);
}
 
